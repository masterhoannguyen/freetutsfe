package fa.training.pojo;



import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountPojo {
	
	private String username;

	private String password;

	private String role;

	private String fullname;

	private String email;

	private String current_job;

	private String writing_field;
}


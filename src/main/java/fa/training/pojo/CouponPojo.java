package fa.training.pojo;




import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor

public class CouponPojo {
	
    private String coupon_name;
    
    private String code;
    
    private String percent;
    
    private String website_url;
	
    
    
}

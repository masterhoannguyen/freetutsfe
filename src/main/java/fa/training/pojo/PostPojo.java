package fa.training.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PostPojo {
	
	private String title;
	
	private String content;
				
}

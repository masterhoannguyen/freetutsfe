package fa.training.pojo;


import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class ReportPojo {
	
    private String location;
    private String message;
    private String status;
    
}

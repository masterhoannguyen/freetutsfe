package fa.training.model;

import javax.persistence.Entity;
import lombok.Data;

/**
 * 
 * @author HoanNV15
 *
 */

@Entity
@Data
public class Account {


	private String username;

	private String password;

	private String role;

	private String fullname;

	private String email;

	private String current_job;

	private String writing_field;
}

package fa.training.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import fa.training.service.UserDetailService;

/**
 * 
 * @author HoanNV15
 *
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailService service;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	};

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(service);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(authenticationProvider());
	}
	
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/add","/admin").hasRole("ADMIN").antMatchers("/post","/checkout")
				.hasAnyRole("ADMIN", "CUSTOMER")
				.antMatchers("/views", "/resources/**", "/detail", "/cartbean", "/login", "/register","/report/saveReport","/saveReport").permitAll().and()
				.formLogin().loginPage("/login.html").usernameParameter("username").passwordParameter("pwd")
				.loginProcessingUrl("/login").permitAll().defaultSuccessUrl("/admin", true)
				.failureUrl("/login-error.html").and().logout().logoutUrl("/logout").logoutSuccessUrl("/").permitAll()
				.and().csrf().disable();

	}
}

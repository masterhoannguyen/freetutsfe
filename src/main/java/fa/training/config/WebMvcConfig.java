package fa.training.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import lombok.experimental.var;

/**
 * 
 * @author HoanNV15
 *
 */
@SuppressWarnings("deprecation")
@EnableWebMvc // mvc:annotation-driven
@Configuration
@ComponentScan({ "fa.training" })
public class WebMvcConfig implements WebMvcConfigurer {

	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * RestTemplate
	 * 
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate() {

		var factory = new SimpleClientHttpRequestFactory();
		factory.setConnectTimeout(3000);
		factory.setReadTimeout(3000);
		return new RestTemplate(factory);
	}

	/**
	 * SpringResourceTemplateResolver
	 * 
	 * @return
	 */
	@Bean
	public SpringResourceTemplateResolver templateResolver() {
		// SpringResourceTemplateResolver automatically integrates with Spring's own
		// resource resolution infrastructure, which is highly recommended.
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setApplicationContext(this.applicationContext);
		templateResolver.setPrefix("/WEB-INF/views/");
		templateResolver.setSuffix(".html");
		templateResolver.setCharacterEncoding("UTF-8");
		// HTML is the default value, added here for the sake of clarity.
		templateResolver.setTemplateMode(TemplateMode.HTML);
		// Template cache is true by default. Set to false if you want
		// templates to be automatically updated when modified.
		templateResolver.setCacheable(false);
		return templateResolver;

	}

	/**
	 * SpringTemplateEngine
	 * 
	 * @return
	 */
	@Bean
	public SpringTemplateEngine templateEngine() {
		// SpringTemplateEngine automatically applies SpringStandardDialect and
		// enables Spring's own MessageSource message resolution mechanisms.
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver());
		// Enabling the SpringEL compiler with Spring 4.2.4 or newer can
		// speed up execution in most scenarios, but might be incompatible
		// with specific cases when expressions in one template are reused
		// across different data types, so this flag is "false" by default
		// for safer backwards compatibility.
		templateEngine.setEnableSpringELCompiler(true);
		return templateEngine;
	}

	/**
	 * ThymeleafViewResolver
	 * 
	 * @return
	 */
	@Bean
	public ViewResolver viewResolver() {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(templateEngine());
		viewResolver.setCharacterEncoding("UTF-8");
		return viewResolver;
	}

	/**
	 * Declare static resource.
	 * 
	 * @param registry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("/resources/static/css**").addResourceLocations("/resources/static/css");
		registry.addResourceHandler("/resources/static/vendors**").addResourceLocations("/resources/static/vendors");
		registry.addResourceHandler("/resources/static/vendors/css**").addResourceLocations("/resources/static/vendors/css");
		registry.addResourceHandler("/resources/static/images**").addResourceLocations("/resources/static/images");
		registry.addResourceHandler("/resources/static/js**").addResourceLocations("/resources/static/js");
		registry.addResourceHandler("/resources/static/css/maps**").addResourceLocations("/resources/static/css/map");
		registry.addResourceHandler("resources/static/vendors/iconfonts/mdi/css**").addResourceLocations("resources/static/vendors/iconfonts/mdi/css");	
		registry.addResourceHandler("/libraries/**").addResourceLocations("/libraries");
		registry.addResourceHandler("/adminPage/account/**").addResourceLocations("/adminPage/account");
	}
}
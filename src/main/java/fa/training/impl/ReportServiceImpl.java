package fa.training.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import fa.training.pojo.ReportPojo;
import fa.training.service.ReportService;

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public void save(ReportPojo report) {
		// request url
		String url = "http://localhost:8081/report/saveReport";
		// create headers
		HttpHeaders headers = new HttpHeaders();
		// set `content-type` header
		headers.setContentType(MediaType.APPLICATION_JSON);
		// set `accept` header
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		// request body parameters

		Map<String, Object> map = new HashMap<>();
		map.put("location", report.getLocation());
		map.put("message", report.getMessage());
		map.put("status", "ERROR");

		// build the request
		HttpEntity<Map<String, Object>> entity = new HttpEntity<Map<String, Object>>(map, headers);
		// send POST request
		restTemplate.postForEntity(url, entity, String.class);
//		ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
		// check response
//		if (response.getStatusCode() == HttpStatus.CREATED) {
//		    System.out.println("Request Successful");
//		    System.out.println(response.getBody());
//		} else {
//		    System.out.println("Request Failed");
//		    System.out.println(response.getStatusCode());
//		}

	}

}

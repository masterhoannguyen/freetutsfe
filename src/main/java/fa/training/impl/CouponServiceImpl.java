package fa.training.impl;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fa.training.pojo.CouponPojo;
import fa.training.service.CouponService;

@Service
public class CouponServiceImpl implements CouponService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<CouponPojo> listCoupon() {
		String url = "http://localhost:8081/coupon/getAll";
		ParameterizedTypeReference<List<CouponPojo>> parameterizedTypeReference = new ParameterizedTypeReference<List<CouponPojo>>() {
		};
		ResponseEntity<List<CouponPojo>> exchange = restTemplate.exchange(url, HttpMethod.GET, null,
				parameterizedTypeReference);
		List<CouponPojo> coupons = exchange.getBody();
		return coupons;
	}

	@Override
	public void saveCoupon(CouponPojo coupon) {
		// TODO Auto-generated method stub

	}



}

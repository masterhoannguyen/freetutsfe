package fa.training.impl;

import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import fa.training.model.Account;
import fa.training.pojo.AccountPojo;
import fa.training.service.AccountService;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Account> listAccountMEO() {
		String url = "http://localhost:8081/account/getAll";
		ParameterizedTypeReference<List<Account>> parameterizedTypeReference = new ParameterizedTypeReference<List<Account>>() {
		};
		ResponseEntity<List<Account>> exchange = restTemplate.exchange(url, HttpMethod.GET, null,
				parameterizedTypeReference);
		List<Account> accounts = exchange.getBody();

		System.out.println(accounts);
		return accounts;

	}

	@Override
	public void saveAccount(Account account) {
		// TODO Auto-generated method stub

	}

}

package fa.training.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fa.training.pojo.PostPojo;
import fa.training.service.PostService;

@Service
public class PostServiceImpl implements PostService {
	@Autowired
	private RestTemplate restTemplate;
	@Override
	public void savePost(PostPojo post) {
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		HttpHeaders headers = new HttpHeaders();
		Calendar cal = Calendar.getInstance();

		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		String uri = "http://localhost:8081/post/saveByCategory";
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		map.put("title", "Test");
		map.put("content", post.getContent());
		map.put("createdDate", sdf.format(cal.getTime()));
		map.put("categoryId", 1);
		//authentication.getName()
		map.put("authorUsername", "author");
		map.put("status", "PENDING");

		HttpEntity<Map<String, Object>> entity = new HttpEntity<Map<String, Object>>(map, headers);

		restTemplate.postForEntity(uri, entity, String.class);
	}

}

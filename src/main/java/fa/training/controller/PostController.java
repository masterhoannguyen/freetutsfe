package fa.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fa.training.pojo.PostPojo;
import fa.training.service.PostService;

@Controller
public class PostController {

	@Autowired
	private PostService service;
	
	@GetMapping(value = "/post")
	public ModelAndView postHome() {
		return new ModelAndView("adminPage/addPost","post",new PostPojo());
	}
	
	@PostMapping(value = "/savePost")
	public String savePost(@ModelAttribute("post") PostPojo post ) {
		service.savePost(post);
		return "redirect:/post";
	}
}

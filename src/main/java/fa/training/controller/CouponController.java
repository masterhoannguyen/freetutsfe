package fa.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fa.training.model.Coupon;
import fa.training.pojo.CouponPojo;
import fa.training.service.CouponService;

@Controller
public class CouponController {

	@Autowired
	private CouponService service;

	@GetMapping(value = "/saveCoupon")
	public ModelAndView couponHome(Coupon coupon) {
		ModelAndView mav = new ModelAndView("saveCoupon");
		mav.addObject("coupons", new Coupon());
		return mav;
	}

	@PostMapping(value = "/savecoupon")
	public String submitcoupon(@ModelAttribute("coupon") CouponPojo coupon, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			System.out.println("Error");
		}
		service.saveCoupon(coupon);
		return "redirect:/";
	}

	@GetMapping(value = { "/listCoupon" })
	public String listCoupon(Model model) {
		model.addAttribute("couponList", service.listCoupon());
		return "redirect:/";
	}

	@GetMapping("/addcoupon")
	public String addcoupon(Model model) {
		model.addAttribute("coupon", new Coupon());
		return "addCoupon";
	}

}
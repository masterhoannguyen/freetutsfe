package fa.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import fa.training.model.Report;
import fa.training.pojo.ReportPojo;
import fa.training.service.ReportService;


@Controller
public class ReportController {
	
	@Autowired
	private ReportService service;
	
	
	@GetMapping(value = "/saveReport")
	public ModelAndView reportHome(Report report) {
	    ModelAndView mav = new ModelAndView("saveReport");
	    mav.addObject("report", new Report());
	    return mav;
	}

	
	@PostMapping(value = "/saveReport")
	public String submitReport(@ModelAttribute("report") ReportPojo report, BindingResult result, ModelMap model) {
		        if (result.hasErrors()) {
		            System.out.println("Error");
		        }       
//		        model.addAttribute("location", report.getLocation());
//		        model.addAttribute("message", report.getMessage());
//		        model.addAttribute("status", "NOT FIX");
		        service.save(report);
		        return "redirect:/";
		    }
  
	
	@RequestMapping(value = "/adminPage/reports")
	   public String getAccountList() {
	     
	      
	      return "adminPage/report/reports";
	   }

}
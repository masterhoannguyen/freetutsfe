package fa.training.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {


	@RequestMapping(value = "/admin")
	public String adminHome() {
		return "adminPage/adminPage";
	}
	
	@RequestMapping(value = "/adminPage/roles")
	public String adminPageRole() {
		return "adminPage/role/roles";
	}
	
	@RequestMapping(value = "/adminPage/users")
	public String adminUser() {
		return "adminPage/user/users";
	}
	
	@RequestMapping(value = "/adminPage/users/newUser")
	public String adminNewUser() {
		return "adminPage/user/newUser";
	}
	

	

	}


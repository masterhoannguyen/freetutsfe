package fa.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import fa.training.model.Coupon;
import fa.training.model.Report;
import fa.training.service.CouponService;

@Controller
public class HomeController {
	@Autowired
	private CouponService service;
	


	@RequestMapping("/")
	public String index(final Model model) {
		model.addAttribute("report", new Report());
		model.addAttribute("coupon", new Coupon());

		model.addAttribute("couponList", service.listCoupon());

		System.out.println("Hello");
		return "home";
	}

}
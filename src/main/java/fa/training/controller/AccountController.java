package fa.training.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import fa.training.impl.AccountServiceImpl;
import fa.training.model.Account;
import fa.training.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountSService;

	/**
	 * Login Form
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login")
	public String loginHome() {
		return "login";
	}

	/**
	 * Login form with error
	 * 
	 * @param model
	 * @return
	 */

	@RequestMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login";
	}

	@GetMapping(value = "/listAccount")
	public String listAccount(Model model) {
		model.addAttribute("accountList", accountSService.listAccountMEO());
		System.out.println("Account");
		return "adminPage/account/accounts";
	}

//	@RequestMapping(value = "/adminPage/accounts/{username}", method = RequestMethod.DELETE)
//	public String deleteAccount(@PathVariable("username") String username) {
//		HttpHeaders headers = new HttpHeaders();
//		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//		HttpEntity<Account> entity = new HttpEntity<Account>(headers);
//
//		return restTemplate
//				.exchange("http://localhost:8080/accounts/" + username, HttpMethod.DELETE, entity, String.class)
//				.getBody();
//	}

	/**
	 * register
	 * 
	 * @param username
	 * @param password
	 * @param role
	 * @param fullname
	 * @param email
	 * @param currentjob
	 * @param writingfield
	 */
//	@GetMapping(value = "/register/{username}/{password}/{role}/{fullname}/{email}/{currentjob}/{writingfield}")
//	public void register(@PathVariable String username, @PathVariable String password, @PathVariable String role,
//			@PathVariable("fullname") String fullname, @PathVariable String email, @PathVariable String currentjob,
//			@PathVariable String writingfield) {
//		String uri = "http://localhost:8081/account/register";
//		Account accountPojo = new Account(username, password, role, fullname, email, currentjob, writingfield);
//		System.out.println("AccountPojo " + accountPojo);
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("currentJob", accountPojo.getCurrentJob());
//		map.put("email", accountPojo.getEmail());
//		map.put("fullName", accountPojo.getFullName());
//		map.put("password", accountPojo.getPassword());
//		map.put("role", accountPojo.getRole());
//		map.put("username", accountPojo.getUsername());
//		map.put("writingField", accountPojo.getWritingField());
//
//		HttpEntity<Map<String, String>> entity = new HttpEntity<Map<String, String>>(map, headers);
//		restTemplate.postForEntity(uri, entity, String.class);
//		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

//		String accountJson = gson.toJson(accountPojo);
//
//		restTemplate.postForObject(uri, accountJson, String.class);

//	}
}

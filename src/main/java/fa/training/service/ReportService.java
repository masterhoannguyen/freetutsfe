package fa.training.service;


import org.springframework.stereotype.Service;
import fa.training.pojo.ReportPojo;

@Service("reportService")
public interface ReportService {

	void save(ReportPojo report);
}

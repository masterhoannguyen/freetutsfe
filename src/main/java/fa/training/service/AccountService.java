package fa.training.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import fa.training.model.Account;
import fa.training.pojo.AccountPojo;

@Service
@Component
public interface AccountService {
	
	public void saveAccount(Account account);
	public List<Account> listAccountMEO();


}

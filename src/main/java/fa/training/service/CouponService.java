package fa.training.service;

import java.util.List;

import org.springframework.stereotype.Service;


import fa.training.pojo.CouponPojo;

@Service("couponService")
public interface CouponService {
	
	public void saveCoupon(CouponPojo coupon);
	public List<CouponPojo> listCoupon();

}

package fa.training.service;

import org.springframework.stereotype.Service;

import fa.training.pojo.PostPojo;

@Service("postService")
public interface PostService {
	
	public void savePost(PostPojo post);
}

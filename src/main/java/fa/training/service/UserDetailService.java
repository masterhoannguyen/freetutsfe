package fa.training.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import fa.training.dto.AccountDetailsDTO;
import fa.training.model.Account;

@Service
public class UserDetailService implements UserDetailsService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String uri = "http://localhost:8081/account/login/" + username;
		String accountJson = restTemplate.getForObject(uri, String.class);
		Account accountPojo = new Gson().fromJson(accountJson, Account.class);

		System.out.println("Account: " + accountJson);
		System.out.println("Hello");

		if (accountPojo == null) {
			throw new UsernameNotFoundException("User not found");
		}
		return new AccountDetailsDTO(accountPojo);
	}

}
